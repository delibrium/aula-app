# Running the Aula Server

There are two ways of running aula backend. The first one you will need Postgres database (version 11 or higher),
[PostgREST 6](https://github.com/PostgREST/postgrest/releases/tag/v6.0.2) and a webserver as Nginx or Apache.

# Docker/Git (preferred method)

You can run aula backend using [Docker Compose](https://docs.docker.com/compose/) following the steps.
Assuming both project will be placed in the same directory.

## Frontend preparation

1. Clone the frontend repository from https://gitlab.com/delibrium/aula-app.git
    ```shell
    git clone https://gitlab.com/delibrium/aula-app.git
    ```

2. `cd aula-app` and install dependencies with `npm i`

3. `cd static/js` and change the base url in `config.js` to `http://localhost:8082/api/`
    ```js
    baseUrl = 'http://localhost:8082/api/'
    ```

4. `cd ../../config/` and change the `BASE_API` in `prod.env.js` to `http://localhost:8082/api/`
    ```js
    module.exports = {
      NODE_ENV: '"production"',
      BASE_API: '"http://localhost:8082/api/"',
      assetsPublicPath: './',
      assetsSubDirectory: './'
    }
    ```

5. `cd ../` and `npm build` to build the frontend into folder `www`

6. Leave the frontend folder with `cd ../`

## Backend

1. Clone the repository from https://gitlab.com/delibrium/delibrium-postgrest.git
    ```shell
    git clone https://gitlab.com/delibrium/delibrium-postgrest.git
    ```

2. `cd delibrium-postgrest` and copy the `www` frontend folder with `cp ../aula-app/www/ ./www -r`

3. `docker-compose up`

4. Access your local instance of Aula opening your browser at http://localhost:8082.
You can select the school aula, username `admin` with password `password`.

# Manual

First you will need to have an superuser on Postgres to create the users and schemas necessary for aula. You can
create them on psql with:

```
$ psql
postgres=# create user aula with superuser ;
CREATE ROLE
postgres=# alter user aula with password 'password';
ALTER ROLE
postgres=# create database aula ;
CREATE DATABASE
```

After that you can create the database running the script `create-database.sh` in this repository.
First create a configuration file as the example `delibrium.postgresql.conf.sample` and run the script with:

`$ ./create-database.sh -u aula -h localhost -p 5435 -d aula -c delibrium.postgresql.conf.sample`

Where `-u` is the user that you created in the step before (aula), `-h` is the address of the postgres server,
`-p` is the port os the postgres server, `-d` is the database name and `-c` is the configuration file.

After that edit the file `delibrium.postgrest.conf.sample' changing the parameters equal the ones creates in the
earlier steps, download the PostgREST executable and run it with:

$ postgrest delibrium.postgres.conf.sample
