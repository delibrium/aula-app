const user = {
  state: {
    profile: {
      roles: {},
      permissions: {},
      is_admin: false,
      is_school_admin: false
    },
    locale: 'de',
    role_switcher_active: false
  },

  mutations: {
    SET_USER: (state, user) => {
      state.profile = user
    },
    SET_EMAIL: (state, email) => {
      state.profile.email = email
    },
    SET_LOCALE: (state, locale) => {
      state.locale = locale
    },
    // eslint-disable-next-line camelcase
    SET_ROLE: (state, roles) => {
      // eslint-disable-next-line camelcase
      state.profile.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.profile.permissions = permissions
    },
    // eslint-disable-next-line camelcase
    SET_ROLE_SWITCHER: (state, role_switcher_active) => {
      // eslint-disable-next-line camelcase
      state.role_switcher_active = role_switcher_active
    }
  },

  actions: {
  }
}

export default user
