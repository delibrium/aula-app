const ui = {
  state: {
    loading: false
  },

  mutations: {
    SET_LOADING: (state, loadingState) => {
      state.loading = loadingState
    }
  }
}

export default ui
