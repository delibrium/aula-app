import service from '@/api/service'

function getAssignedRoles (userId, schoolId) {
  const params = {
    user_id: `eq.${userId}`,
    school_id: `eq.${schoolId}`
  }
  return service.get('/user_role', {params})
}

function assignRole (role) {
  return service.post('/user_role', role)
}

function deleteRole (userId, roleId, schoolId, ideaSpace) {
  if (ideaSpace == null) {
    const params = {
      user_id: `eq.${userId}`,
      role_id: `eq.${roleId}`,
      school_id: `eq.${schoolId}`,
      idea_space: 'is.null'
    }
    return service.delete('/user_role', {params})
  } else {
    const params = {
      user_id: `eq.${userId}`,
      role_id: `eq.${roleId}`,
      school_id: `eq.${schoolId}`,
      idea_space: `eq.${ideaSpace}`
    }
    return service.delete('/user_role', {params})
  }
}

export default {
  getAssignedRoles,
  assignRole,
  deleteRole
}
