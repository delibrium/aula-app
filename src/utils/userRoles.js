import api from '../api'
import store from '../store'

export async function getUserRoles () {
  return api.roles.getRoles(store.getters.selected_school).then(res => {
    if (res.status < 400) {
      return res.data
    }
    return []
  })
}
